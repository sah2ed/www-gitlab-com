---
layout: markdown_page
title: "People Operations Administrator"
---

## Responsibilities

* Assist in smooth onboarding of new team members from the administrative side.
* Process changes to team members compensation, position, etc.
* Provide assistance to the team with miscellaneous support tasks. 
